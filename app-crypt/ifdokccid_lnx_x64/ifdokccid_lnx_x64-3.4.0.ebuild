# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_FILE="${P}.tar.gz"

DESCRIPTION="PC/SC driver for CardMan 1021, 3021, 3121, 3621, 3821, 4321, 6121, Smart@Link, Smart@Key"
HOMEPAGE="http://www.omnikey.com"
SRC_URI="http://www.omnikey.com/index.php?id=69&rName=CardMan%203x21%20PC/SC%20CCID%20for%20Linux%20X64&did=69&file=${MY_FILE}"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/pcsc-lite"

#src_unpack() {
#	unpack ${MY_FILE}
#}

src_install() {
	insinto /usr/lib64/readers/usb/
	doins -r ${P}.bundle

	insinto /etc/udev/rules.d
	sed -i z98_omnikey.rules -e 's:/usr/local/sbin/:/usr/sbin/:'
	sed -i z98_omnikey.rules -e 's:}=":}==":'
	newins z98_omnikey.rules 98-omnikey.rules
}

