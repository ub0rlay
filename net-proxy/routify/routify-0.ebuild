# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="userland routing"
HOMEPAGE="NULL"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

EPEND=""
RDEPEND="net-analyzer/netcat
	net-proxy/nylon"

src_install() {
	newbin ${FILESDIR}/${PN}-${PV} ${PN}
}
