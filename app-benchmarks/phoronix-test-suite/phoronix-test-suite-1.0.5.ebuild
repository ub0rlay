# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# TODO:
# * add installation of new documenation (README.html and documentation/) as real dodoc,
#   currently docs end inside the normal install path (/usr/share/phoronix-test-suite/documentation/)
# * switch SRC_URI to http://www.phoronix-test-suite.com/download.php?file=${P}.tar.gz
#   currently this is not done due to problems with the filename in src_unpack, but
#   thic change would allow using existing mirros for the package

inherit eutils depend.php

DESCRIPTION="Comprehensive testing and benchmarking platform"
HOMEPAGE="http://phoronix-test-suite.com"
SRC_URI="http://www.phoronix-test-suite.com/releases/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="get-all-deps"

RDEPEND="sys-process/time
		sys-apps/lsb-release
	get-all-deps? ( app-shells/tcsh
		dev-util/git
		dev-util/scons
		media-libs/glew
		media-libs/imlib2
		media-libs/jpeg
		>=media-libs/libpng-1.2
		>=media-libs/libsdl-1.2
		media-libs/portaudio
		>=media-libs/sdl-gfx-2.0
		>=media-libs/sdl-image-1.2
		>=media-libs/sdl-net-1.2
		>=media-libs/sdl-ttf-2.0
		sys-devel/bison
		sys-devel/flex
		virtual/glut
		>=virtual/jre-1.5
		x11-base/xorg-server
		>=x11-libs/gtk+-2 ) "
need_php_cli

S="${WORKDIR}/${PN}"

pkg_setup() {
	has_php
	require_php_with_use cli gd
	if use get-all-deps && ! built_with_use sys-devel/gcc fortran ; then
		echo
		eerror "In order to be able to install all tests for the phoronix-test-suite you need to have"
		eerror "sys-devel/gcc emerged with \"fortran\" in your USE flag. Please add"
		eerror "that flag, re-emerge gcc, and then emerge phoronix-test-suite again."
		echo
		die "sys-devel/gcc not built with \"fortran\" support."
	fi
}


src_unpack() {
	unpack ${A}
	cd ${S}
	sed -i -e "s,export PTS_DIR=\`pwd\`,export PTS_DIR=\"/usr/share/${PN}\"," ${PN}
}

src_install() {
	dodir /usr/share/${PN}
	insinto /usr/share/${PN}
	exeinto /usr/bin
	doins -r ${S}/{documentation,pts,pts-core,README.html} || die "Install failed!"
	fperms 755 /usr/share/${PN}/pts-core/scripts/launch-browser.sh
	fperms 755 /usr/share/${PN}/pts/distro-scripts/*.sh
	doexe phoronix-test-suite || die "Installing the executable failed!"
	dodoc CHANGE-LOG
}

pkg_postinst() {
	if use ! get-all-deps ; then
		elog "For several tests external dependencies are needed. You can"
		elog "easily install them with setting the useflag 'get-all-deps'."
		elog "When not having the deps installed, the testsuite will ask for"
		elog "the root password when installing tests that require external"
		elog "dependencies and install them systemwide via portage."
		elog "These are the possible deps:"
		elog "app-shells/tcsh"
		elog "dev-util/git"
		elog "dev-util/scons"
		elog "media-libs/glew"
		elog "media-libs/imlib2"
		elog "media-libs/jpeg"
		elog ">=media-libs/libpng-1.2"
		elog ">=media-libs/libsdl-1.2"
		elog "media-libs/portaudio"
		elog ">=media-libs/sdl-gfx-2.0"
		elog ">=media-libs/sdl-image-1.2"
		elog ">=media-libs/sdl-net-1.2"
		elog ">=media-libs/sdl-ttf-2.0"
		elog "sys-devel/bison"
		elog "sys-devel/flex"
		elog "sys-devel/gcc with fortran useflag active"
		elog "virtual/glut"
		elog ">=virtual/jre-1.5"
		elog "x11-base/xorg-server"
		elog ">=x11-libs/gtk+-2"
	fi
}
