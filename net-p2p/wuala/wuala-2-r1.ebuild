# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Wuala, your free online hard-disk"
HOMEPAGE="http://wuala.com/"
SRC_URI="http://www.wuala.com/files/wuala.tar.gz"

LICENSE="wuala"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=">=virtual/jre-1.5.0"

src_install() {
	cd ${PN}
	dodir /opt/${PN}
	
	insinto /opt/${PN}
	doins loader2.jar

	exeinto /opt/${PN}
	doexe ${FILESDIR}/wuala ${FILESDIR}/wualacmd
	dosym /opt/${PN}/wuala /opt/bin/wuala
	dosym /opt/${PN}/wualacmd /opt/bin/wualacmd
	
	dodoc readme.txt
}
