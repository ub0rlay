# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit games

DESCRIPTION="crossplatform point'n'click adventure engine in D using sdl"
HOMEPAGE="http://www.leetless.de/indiana/index.html"
SRC_URI="http://www.leetless.de/archives/indiana/releases/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-util/dsss
	${RDEPEND}"

RDEPEND="media-libs/libsdl
media-libs/sdl-image
media-libs/sdl-gfx
media-libs/sdl-ttf
media-libs/sdl-mixer"

pkg_setup(){

	if ! built_with_use sys-devel/gcc d; then
		eerror "                                                 "
		eerror "you need to compile sys-devel/gcc with d useflag "
		eerror "												 "
		die "No suitable D compiler	found!"
	fi
}

src_compile(){
	
	cd ${PN}
	dsss build --prefix=${D}/usr || die "build failed"
}

src_install(){

	cd ${PN}
	dsss install --prefix=${D}/usr || die "install failed"
}
