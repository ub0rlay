# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_PV=${PV:0:4}-${PV:5}
MY_P=${PN}-${MY_PV}

DESCRIPTION="TurboPrint ermöglicht den Einsatz moderner Farbdrucker unter Linux."
HOMEPAGE="http://www.turboprint.de"
SRC_URI="
	http://www.turboprint.info/${MY_P}.x86_64.tgz
	ftp://ftp.zedonet.com/${MY_P}.x86_64.tgz
	http://www.turboprint.info/${MY_P}.tgz
	ftp://ftp.zedonet.com/${MY_P}.tgz
	"

LICENSE="free for homeuse"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="gtk"

DEPEND="sys-apps/grep sys-apps/sed net-print/cups"
RDEPEND="
  gtk? ( =x11-libs/gtk+-1.2* )
  net-print/cups
  "

src_unpack() {
	if echo ${ACCEPT_KEYWORDS} | grep amd64 > /dev/null ; then
		A="${MY_P}.x86_64.tgz"
	else
		A="${MY_P}.tgz"
	fi
	unpack ${A} || die "failed unpacking the source"
	cd ${MY_P}
	epatch ${FILESDIR}/setup-0.patch
	epatch ${FILESDIR}/install-static-0.patch
	epatch ${FILESDIR}/install-post-0.patch
	sed -i lib/install-post -e 's/"$TPPATH/"$RBR$TPPATH/g' || die "failed patching installer"
	sed -i lib/install-post -e 's/"\/usr/"$RBR\/usr/g' || die "failed patching installer"
	sed -i lib/install-post -e 's/"\/etc/"$RBR\/etc/g' || die "failed patching installer"
	sed -i lib/install-post -e 's/ \/etc/ $RBR\/etc/g' || die "failed patching installer"
	sed -i lib/install-post -e 's/^tpsetup/#tpsetup/g' || die "failed patching installer"
	sed -i lib/install-info -e 's/"$TPPATH/"$RBR$TPPATH/g' || die "failed patching installer"
}

src_install() {
	my_args="--batch --cups"
	use gtk || my_args="$my_args --nogui"
	cd ${MY_P}
	export D
	export RBR=${D}

	exeinto /usr/libexec/cups/filter/
	doexe lib/pstoturboprint
	doexe lib/rastertoturboprint

	#rm lib/pstoturboprint
	#rm lib/rastertoturboprint

	mkdir -p "$D/usr/share/applications/"
	mkdir -p "$D/usr/kde/3.5/share/applnk/Utilities/"

	./setup $my_args

	sed -i "$D/etc/system.cfg" -e 's:.*//:/:'

	elog "please run tpsetup --writeppdfiles \"/usr/share/ppd/\""
	elog "please run tpsetup --update and restart cups"
}
