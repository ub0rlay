# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit eutils subversion

DESCRIPTION="Encrypted Filesystem module for Linux"
ESVN_REPO_URI="http://encfs.googlecode.com/svn/trunk/"
ESVN_PROJECT="encfs"
ESVN_BOOTSTRAP="sh ./reconfig.sh"
HOMEPAGE="http://arg0.net/encfs"
LICENSE="GPL-2"
KEYWORDS=""
SLOT="0"
IUSE="nls"

DEPEND=">=dev-libs/openssl-0.9.7
	>=sys-fs/fuse-2.6
	>=dev-libs/rlog-1.3.6
	>=dev-libs/boost-1.34
	nls? ( >=sys-devel/gettext-0.14.1 )"

src_compile() {
	libtoolize --copy --force
	econf `use_enable nls` --with-boost-filesystem=boost_filesystem-mt || die
	emake || die
}

src_install() {
	make DESTDIR="${D}" install || die "make install failed"
	dodoc AUTHORS ChangeLog README
}

pkg_postinst() {
	einfo "Please see http://www.arg0.net/encfsintro"
	einfo "if this is your first time using encfs."
}
