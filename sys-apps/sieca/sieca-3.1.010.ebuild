# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="HiPath SIcurity Card API"
HOMEPAGE="http://www.cms.hu-berlin.de/dl/zertifizierung/SC/Einsatz/Install-Linux_html"
SRC_URI="ftp://hu-ftp.hu-berlin.de/pub/smartcard/Software/Linux/HiPath_SIcurity_Card_API_V3_1_010_Linux.tar.gz"

LICENSE="Siemens License"
SLOT="0"
KEYWORDS="x86"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/pcsc-lite"

src_install() {
	sed -i 's:/usr/local:/usr/share:' etc/*
	insinto /etc
	doins etc/*
	dobin usr/local/bin/*
	dolib usr/local/lib/*
	insinto /usr/share/${PN}/scripts
	doins usr/local/${PN}/scripts/*

	einfo "link libpcsclite.so.0 to libpcsclite.so.1.0.0"
}
