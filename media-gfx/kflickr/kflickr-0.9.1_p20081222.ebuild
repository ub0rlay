# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
 
EAPI="2"

NEED_KDE="4.2"
inherit kde4-base

#KDE_LINGUAS="de es fr it"

DESCRIPTION="KDE App for easy upload of your favourite photos to your Flickr.com account"
HOMEPAGE="http://kflickr.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-20081222.tar.bz2"

SLOT="4"
KEYWORDS="~amd64 ~x86"
LICENSE="GPL-2"
IUSE="debug"

S="${WORKDIR}/${PN}-20081222"
